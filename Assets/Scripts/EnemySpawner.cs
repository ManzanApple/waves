﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    public GameObject[] enemiesPrefabs;

    public float spawnArea = 15;

    public bool showSpawnGizmo = false;

    private SpectrumAnalyser _spectrum;

    private float lastMax;

    void Awake()
    {
        _spectrum = gameObject.GetComponent<SpectrumAnalyser>();
    }


    void Update()
    {
        var actualMax = _spectrum.GetMax();

        if (lastMax != actualMax)
        {
            if (actualMax > 90)
                SpawnEnemy(enemiesPrefabs[1]);
            else if (actualMax > 70)
                SpawnEnemy(enemiesPrefabs[1]);
            else if (actualMax > 50)
                SpawnEnemy(enemiesPrefabs[0]);

            lastMax = actualMax;
        }
    }

    void SpawnEnemy(GameObject prefab)
    {
        if (enemiesPrefabs[0])
        {
            var randomPosition = Random.insideUnitCircle * spawnArea;
            
            var newEnemy = (GameObject)GameObject.Instantiate(prefab, new Vector3(randomPosition.x, 0, randomPosition.y), Quaternion.identity);

            /*var enemyRenderer = newEnemy.GetComponent<MeshRenderer>();
            enemyRenderer.material.color = newColor;*/
        }
    }

    void OnDrawGizmos()
    {
        if(showSpawnGizmo)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(Vector3.zero, spawnArea);
        }
    }
}
