﻿using UnityEngine;
using System.Collections;

public class CameraSmoothFollow : MonoBehaviour {

    public GameObject objetive;

    public Vector3 clamp;

    public bool isFollowing;
    	
	void FixedUpdate()
    {
        if (isFollowing && objetive)
        {
            var velocity = Vector3.zero;
            
            var tmpObjetiveVector = new Vector3(
                Mathf.Clamp(objetive.transform.position.x, -clamp.x, clamp.x),
                clamp.y,
                Mathf.Clamp(objetive.transform.position.z, -clamp.z, clamp.z)
            );
            gameObject.transform.position = Vector3.SmoothDamp(gameObject.transform.position, tmpObjetiveVector,ref velocity, 0.03f);
        }
	}
}
