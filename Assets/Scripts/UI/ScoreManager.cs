﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public static ScoreManager instance;

    public Text scoreText;

    private int _score;

    void Awake()
    {
        instance = this;
    }

    void UpdateScore()
    {
        print(scoreText);
        if (!scoreText) return;

        scoreText.text = _score.ToString();
    }

    public void AddScore(int points)
    {
        _score += points;

        UpdateScore();
    }

    public void RemoveScore(int points)
    {
        _score = Mathf.Max(0, _score - points);

        UpdateScore();
    }
}
