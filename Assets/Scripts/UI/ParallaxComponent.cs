﻿using UnityEngine;
using System.Collections;

public class ParallaxComponent : MonoBehaviour {

    public float speed = 500f;

    private Vector3 _origin;

    void Awake()
    {
        _origin = transform.localPosition;
    }

	void Update ()
    {

        var horizontalMove = Vector3.right * -Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        var verticalMove = Vector3.up * -Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.localPosition = Vector3.Lerp(_origin, _origin + (horizontalMove + verticalMove), 0.3f);
    }
}
