﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

    public float slowPitch = 0.7f;
    private float actualPitch = 1.0f;

    private AudioSource _audioSorce;

    void Awake()
    {
        _audioSorce = gameObject.GetComponent<AudioSource>();

        if (!_audioSorce) gameObject.AddComponent<AudioSource>();
    }


    void Update()
    {
        // Correccion para que no se escuche mal
        if (Time.timeScale < 0.9f)
            actualPitch = slowPitch;
        else
            actualPitch = 1.0f;

        _audioSorce.pitch = Mathf.Lerp(_audioSorce.pitch, actualPitch, 0.05f);

        if (!_audioSorce.isPlaying && LevelManager.instance != null)
        {
            LevelManager.instance.LoadScene(SceneID.MENU, true);
        }
    }
}
