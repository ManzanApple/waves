﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour
{
    public float speed = 5;

    public bool isDead = false;

    public GameObject hitSoundEffect;


    private Rigidbody _rigidbody;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();

        if (_rigidbody == null) _rigidbody = gameObject.AddComponent<Rigidbody>();

        Physics.IgnoreLayerCollision(8, 10, true);
    }

    void FixedUpdate()
    {
        var horizontal  = Input.GetAxis("Horizontal");
        var vertical    = Input.GetAxis("Vertical");

        _rigidbody.MovePosition(transform.position + ((transform.forward * vertical) + (transform.right * horizontal))
                                * Time.deltaTime * speed);
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.layer == 9)
        {
            Instantiate(hitSoundEffect, transform.position, Quaternion.identity, transform);
            ScoreManager.instance.RemoveScore(500);
        }
    }
}
