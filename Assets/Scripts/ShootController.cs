﻿using UnityEngine;
using System.Collections;

public class ShootController : MonoBehaviour {

    public GameObject bulletPrefab;

    public float timeToShoot = 0.3f;
    public int shootAngle = 10;

    private float currentTimeToShoot;
    private Vector3 targetPosition;


    void Awake()
    {
        currentTimeToShoot = timeToShoot;
    }

    void Update()
    {
        currentTimeToShoot -= Time.deltaTime;

        UpdateArrow();

        if (Input.GetMouseButton(0) && currentTimeToShoot < 0)
        {
            Shoot();
            currentTimeToShoot = timeToShoot;
        }
    }

    void UpdateArrow()
    {
        float hitdist = 0f;

        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        playerPlane.Raycast(ray, out hitdist);

        var targetPoint = ray.GetPoint(hitdist);
        targetPosition = ray.GetPoint(hitdist);

        transform.LookAt(targetPosition);
    }

    void Shoot()
    {
        for (int i = -shootAngle; i <= shootAngle; i += shootAngle)
        {
            var rotation = transform.rotation * Quaternion.Euler(0, i, 0);
            Instantiate(bulletPrefab, transform.position, rotation);
        }
    }

    /*void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, targetPosition);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(targetPosition, 2f);
    }*/
}
