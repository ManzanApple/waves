﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float speed = 10f;

    public float lifeTime = 5f;

    private Rigidbody _rigidbody;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();

        if (_rigidbody == null) _rigidbody = gameObject.AddComponent<Rigidbody>();
    }

    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime < 0) Destroy(gameObject);
    }

    void FixedUpdate()
    {
        _rigidbody.MovePosition(transform.position + transform.forward * Time.deltaTime * speed);
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.layer == 9)
        {
            Destroy(gameObject);
        }
    }
}
