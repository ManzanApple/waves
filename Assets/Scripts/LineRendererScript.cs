﻿using UnityEngine;
using System.Collections;

public class LineRendererScript : MonoBehaviour {

    public GameObject[] points;

    private LineRenderer _lineRender;

    void Awake()
    {
        _lineRender = gameObject.GetComponent<LineRenderer>();
    }

    void Update()
    {
        for (int i = 0; i < points.Length; i++)
        {
            _lineRender.SetPosition(i, points[i].transform.position);
        }
    }
}
