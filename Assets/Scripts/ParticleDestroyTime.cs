﻿using UnityEngine;
using System.Collections;

public class ParticleDestroyTime : MonoBehaviour {

    public float timeToDestroy = 1.0f;

	// Update is called once per frame
	void Update ()
    {
        timeToDestroy -= Time.deltaTime;

        if (timeToDestroy < 0) Destroy(gameObject);
	}
}
