﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    public float speed = 5;

    public int scoreValue = 10;

    public GameObject dieParticle;
    public GameObject dieSound;

    private Rigidbody _rigidbody;

    private GameObject _target;
    private CharacterController _controller;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        if (_rigidbody == null) _rigidbody = gameObject.AddComponent<Rigidbody>();

        _target = GameObject.FindGameObjectWithTag("Player");
        _controller = _target.GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        if (_controller.isDead) return;

        gameObject.transform.LookAt(_target.transform.position);

        _rigidbody.MovePosition(transform.position + (transform.forward * Time.deltaTime * speed));
    }


    void Destroy()
    {
        Instantiate(dieSound, transform.position, Quaternion.identity);

        Instantiate(dieParticle, transform.position, Quaternion.identity);

        ScoreManager.instance.AddScore(scoreValue);

        // GameObject Del Parent
        Destroy(gameObject.transform.parent.gameObject);
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.layer == 8 || c.gameObject.layer == 10) Destroy();
    }
}
