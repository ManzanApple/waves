﻿using UnityEngine;
using System.Collections;

public class SpectrumAnalyser : MonoBehaviour {

    public float timeToGetSamples = 0.5f;

    private float currentTimeToGetSamples;

    // Audio Spectrum Data
    private float[] samples;
    private float max = 0;

    private AudioSource _audioSorce;

    void Awake()
    {
        _audioSorce = gameObject.GetComponent<AudioSource>();

        if (!_audioSorce) gameObject.AddComponent<AudioSource>();

        currentTimeToGetSamples = timeToGetSamples;
    }

    void Update ()
    {
        currentTimeToGetSamples -= Time.deltaTime;

        if (currentTimeToGetSamples < 0)
        {
            AnalyzeSpectrum();
            currentTimeToGetSamples = timeToGetSamples;
        }
	}

    void AnalyzeSpectrum()
    {
        max = 0;
        samples = new float[1024];

        _audioSorce.GetOutputData(samples, 0);


        foreach (float sample in samples)
        {
            if (sample > max) max = sample;
        }
    }

    public float GetMax()
    {
        return max * 100;
    }

    public float[] GetSpectrum()
    {
        return samples;
    }
}
