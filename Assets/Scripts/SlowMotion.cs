﻿using UnityEngine;
using System.Collections;

public class SlowMotion : MonoBehaviour {

    public float slowSpeed = 0.5f;
    public Color slowColor = Color.red;
    public GameObject map;

    private float _actualSpeed = 1.0f;
    private MeshRenderer _mapMesh;
    private Color _actualColor;
    private Color _originalColor;

    void Awake()
    {
        _mapMesh        = map.GetComponent<MeshRenderer>();
        _originalColor  = _mapMesh.materials[1].color;
        _actualColor = _originalColor;
    }

    void Update()
    {
        if (Input.GetMouseButton(1)) SlowDown();

        if (Input.GetMouseButtonUp(1)) SpeedUp();


        _mapMesh.materials[1].color = Color.Lerp(_mapMesh.materials[1].color, _actualColor, 0.05f);

        Time.timeScale = Mathf.Lerp(Time.timeScale, _actualSpeed, 0.25f);
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    void SlowDown()
    {
        _actualSpeed = slowSpeed;

        _actualColor = slowColor;
    }

    void SpeedUp()
    {
        _actualSpeed = 1.0f;

        _actualColor = _originalColor;
    }
}
