﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    public GameObject character;

    public GameObject[] menuPoints;

    public GameObject[] spherePoints;

    public MenuID setMenu = MenuID.INIT;

    public enum MenuID { INIT, PLAY };

    public float speedRotation = 0.5f;
    
    private float[] _fov = new float[2] {40, 32};

    private bool[] _animation = new bool[2] { false, true };

    private Camera _camera;
    private Animator _animator;

    private int _actualmenu = -1;
    void Awake()
    {
        _camera   = gameObject.GetComponent<Camera>();
        _animator = character.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
            setMenu = MenuID.PLAY;
        else if (Input.GetKeyDown(KeyCode.A))
            setMenu = MenuID.INIT;

        var castedsetMenu = (int)setMenu;

        if (menuPoints.Length > 0)
        {

            _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _fov[castedsetMenu], 0.1f);

            var targetRotation = Quaternion.LookRotation(menuPoints[castedsetMenu].transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speedRotation * Time.deltaTime);

            character.transform.parent.position = spherePoints[castedsetMenu].transform.position;
            _actualmenu = castedsetMenu;
            _animator.SetBool("StartJump", _animation[castedsetMenu]);
        }
    }

    public void GoToInit()
    {
        setMenu = MenuID.INIT;
    }

    public void GoToPlay()
    {
        setMenu = MenuID.PLAY;
    }

    public void GoToGame()
    {
        if (LevelManager.instance)
            LevelManager.instance.LoadScene(SceneID.GAME);
        else
            print("Necesitas iniciar el bootstrap para que funcione el lvl manager.");
    }
}